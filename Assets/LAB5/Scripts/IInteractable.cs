using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mathasit.GameDev3.Chapter5.InteractionSystem
 {
     public interface IInteractable
 {
     void Interact(GameObject actor);
     }
 }