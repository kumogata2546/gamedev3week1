using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mathasit.GameDev3.Chapter5.PlayerController
 {
    public interface IPlayerController
 {
     void MoveForward();
     void MoveForwardSprint();
    
     void MoveBackward();
    
     void TurnLeft();
     void TurnRight();
     }
 }
