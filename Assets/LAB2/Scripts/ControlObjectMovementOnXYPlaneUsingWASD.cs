using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;

namespace Mathasit.GameDev3.Chapter2
 {
     public class ControlObjectMovementOnXYPlaneUsingWASD : MonoBehaviour {
 public float m_MovementStep;

 // Use this for initialization
 void Start () {
    
     }
 // Update is called once per frame
 void Update () {
     if (Input.GetKeyDown(KeyCode.A))
         {
         this.transform.Translate(-m_MovementStep , 0, 0);
         }
     else if (Input.GetKeyDown(KeyCode.D))
         {
         this.transform.Translate(m_MovementStep , 0, 0);
         }
     else if (Input.GetKeyDown(KeyCode.W))
     {
          this.transform.Translate(0, m_MovementStep , 0);
          }
      else if (Input.GetKeyDown(KeyCode.S))
          {
          this.transform.Translate(0, -m_MovementStep , 0);
          }
      }
  }
 }
